module.exports = {
  baseUrl: process.env.NODE_ENV === 'production'
    ? '/projets/3/'
    : '/'
}