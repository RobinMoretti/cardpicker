import Vue from 'vue'
import App from './App.vue'

import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.config.productionTip = false

Vue.mixin({
  data: function() {
    return {
    	gringSounds: [
    	'./sounds/1.wav',
    	'./sounds/2.wav',
    	'./sounds/3.wav',
    	'./sounds/4.wav',
    	'./sounds/6.wav',
    	'./sounds/5.wav'
    	],
    	winSound: './sounds/end.wav'
    }
  },

})

new Vue({
  render: h => h(App)
}).$mount('#app')
